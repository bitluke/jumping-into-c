#include <cstdlib>
#include <iostream>
#include <ctime>

using namespace std;
int rand_range(int high , int low);
enum CoinSide { TAIL =  1, HEAD = 2};

int main(){

	srand(time(NULL));
	
	long head = 0;
	long tail = 0;
 	
	for (int i = 0; i < 1000000000 ; ++i){
	
		if(rand_range(3,1) == TAIL){
	
			tail++;
	
		}else {
	
			head++;
	
		}

	}

	//ouput the numbers.
	cout << "head : " << head << " Tail : " << tail << " Total : " << head + tail << endl;

}

int rand_range(int high , int low){
	return rand() % (high - low) + low;
}
