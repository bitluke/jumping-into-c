#include <iostream>

using namespace std;

void menu(){
	cout << "Menu : \n";
        cout << "1 : One \n";
        cout << "2 : two \n";
        cout << "3 : three \n";
        cout << "4 : four \n";
        cout << "5 : five \n";
        cout << "6 : six \n";
        cout << "7 : Seven \n";

}

int  accept_input(){
	int menu_number;
	cin >> menu_number;
	return menu_number;
}

int main(){
	menu();
	int menu_value = accept_input();
	while(menu_value < 1 || menu_value > 7){
		menu();
		menu_value = accept_input();
	}

}
