#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

//######### Enums
enum TicTacToeOption { TTT_EMPTY = '-', TTT_O = 'O', TTT_X = 'X'};


//######### Structs.
struct Player{
    string _name;
    bool _winner;
    bool _is_computer;
    int _position;
    bool _has_played;
    TicTacToeOption _players_symbol ;
};

//######### const declaration
const int ROW_LENGTH = 3;
const int COLUMN_LENGTH = 3;
const string COMPUTER_NAME = "Computer";

//######### Function declaration
void display_player(Player &player);
bool convertYesOrNoToBoolean(char symbol);
string convert_number_toWords(int number);
bool is_name_set(string name);
bool name_exist(string name);
bool is_player_symbol_valid(TicTacToeOption symbol);
void createPlayerDetails(Player& player);
Player createPlayer();
bool no_winner(Player& player, char board[][COLUMN_LENGTH]);
Player updatePlayer(Player player, string name, bool iswinner, TicTacToeOption player_symbol);
void play(int row , int column, TicTacToeOption players_symbol,char board[][COLUMN_LENGTH]);
bool is_position_empty(int row , int column, char board[][COLUMN_LENGTH]);
TicTacToeOption convertToTictacToeSymbol(char symbol);
bool no_winner(char board[][COLUMN_LENGTH]);
int* generate_random_row_column(char board[][COLUMN_LENGTH]);
int generate_random_num_within_range(int high, int low);
bool valid(int row, int column);
int player_count = 0;
void display_winner(Player& player);

TicTacToeOption last_symbol = TTT_EMPTY;
string last_name = "no name";
bool computer_already_selected = false ;

void init_board(char board[][COLUMN_LENGTH]);
void print_board(char board[][COLUMN_LENGTH]);


int main(int argc, const char * argv[]){
    
    srand(time(NULL));
    
    char tictactoe_board[ROW_LENGTH][COLUMN_LENGTH];
    
    //Set up board.
    init_board(tictactoe_board);
    int board_row = 0; int board_column = 0;
    
    //create players
    Player player_a = createPlayer();
    cout << convert_number_toWords(player_a._position) <<" player Set up time " << endl;
    createPlayerDetails(player_a);
    
    Player player_b = createPlayer();
    cout << convert_number_toWords(player_b._position) <<" player Set up time " << endl;
    createPlayerDetails(player_b);
    
    cout << "\n \nDisplaying player details \n";
    display_player(player_a);
    display_player(player_b);
    
    cout << endl <<  "****************** Time to play *************************" << endl;
    cout << " specify position in terms of row and column \n ";
    cout << " starting from 1 and the last possible number is  3 \n ";
    cout << " row = 1 , row =  3 , column = 1 , column = 3 \n ";
    
    Player current_player = player_a;
    int number_of_plays = 0;
    
    while(number_of_plays < 9){
        
        do{
            
            if (current_player._is_computer) {
                int* comp_row_col =  generate_random_row_column(tictactoe_board);
                board_row = comp_row_col[0] ;
                board_column = comp_row_col[1];
                cout << "Computer played the following : row " << board_row << " colum : " << board_column << endl;
                delete comp_row_col;
            } else {
                cout << current_player._name << "  your turn " << endl;
                cout << "Enter row : " ;
                cin >> board_row;
                cout << "Enter column : " ;
                cin >> board_column;
            }
            
        }while(!valid(board_row, board_column) || !is_position_empty(board_row, board_column, tictactoe_board));
        
        play(board_row, board_column, current_player._players_symbol, tictactoe_board);
        
        print_board(tictactoe_board);
        
        if (!no_winner(current_player, tictactoe_board)) {
            break;
        }
        
        if (current_player._position == player_a._position) {
            current_player = player_b;
        } else {
            current_player = player_a;
        }
        
        number_of_plays++;
    }
    
    display_winner(current_player);
}

void display_winner(Player& player){
    if (player._winner) {
        cout << player._name << " is the winner \n";
    }else {
        cout << "It is a tie \n";
    }
    
}

bool valid(int row, int column){
    if(row >= 1 && row <= 3 ){
        if(column >= 1 && column <= 3){
            return true;
        }
    }
    return false;
}

void createPlayerDetails(Player& player){
    if (!computer_already_selected) {
        char player_is_computer = 'n';
        cout << "Would you like to be the computer [\"Y\" or \"N\"] : ";
        cin >> player_is_computer;
        
        player._is_computer = convertYesOrNoToBoolean(player_is_computer);
        if (player._is_computer) {
            
            player._name = COMPUTER_NAME;
            computer_already_selected = true;
            
        } else {
            
            while (!is_name_set(player._name) || name_exist(player._name)) {
                cout << "Name : ";
                cin >> player._name ;
            }
            
            computer_already_selected = false;
            
        }
        
    } else {
        
        while (!is_name_set(player._name) || name_exist(player._name)) {
            cout << "Name : ";
            cin >> player._name ;
        }
        
        computer_already_selected = false;
    }
    
    
    
    if (last_symbol == TTT_EMPTY) {
        cout << "What symbol would you like to use [\"X\" or \"x\" or \"O\" or \"o\"] : ";
        char symbol;
        cin >> symbol ;
        player._players_symbol = convertToTictacToeSymbol(symbol);
        
        while (!is_player_symbol_valid(player._players_symbol)) {
            cout << "What symbol would you like to use [\"X\" or \"x\" or \"O\" or \"o\"] : ";
            char symbol;
            cin >> symbol ;
            player._players_symbol = convertToTictacToeSymbol(symbol);
        }
        
    }else{
        
        if (last_symbol == TTT_X) {
            player._players_symbol = TTT_O;
        } else {
            player._players_symbol = TTT_X;
        }
        cout << player._name << " "<< char(player._players_symbol) << " has been choosen for you " << endl;
    }
    
    last_symbol = player._players_symbol;
}

Player createPlayer(){
    Player player ;
    player._name = "";
    player._winner = false;
    player._is_computer = false;
    player._has_played = false;
    player._position = ++player_count;
    player._players_symbol = TTT_EMPTY ;
    return player;
}


string convert_number_toWords(int number){
    switch (number) {
        case 1:
            return "First";
        case 2:
        default:
            return "Second";
    }
}

bool convertYesOrNoToBoolean(char symbol){
    switch (symbol) {
        case 'Y':
        case 'y':
            return true;
        case 'N':
        case 'n':
            return false;
        default:
            return false;
    }
    
}


TicTacToeOption convertToTictacToeSymbol(char symbol){
    switch (symbol) {
        case 'X':
        case 'x':
            return TTT_X;
        case 'O':
        case 'o':
            return TTT_O;
        default:
            return TTT_EMPTY;
    }
    
}

Player updatePlayer(Player player, string name, bool iswinner, TicTacToeOption player_symbol){
    player._name = name;
    player._players_symbol = player_symbol ;
    return player;
}


void init_board(char board[][COLUMN_LENGTH]){
    for(int row = 0; row < ROW_LENGTH; row++){
        for(int column = 0; column < COLUMN_LENGTH; column++){
            board[row][column] = TTT_EMPTY;
        }
    }
}

bool is_name_set(string name){
    if (name.length() > 0) {
        return true;
    }
    return false;
}

bool name_exist(string name){
    return last_name == name;
}


bool is_player_symbol_valid(TicTacToeOption player_symbol){
    return player_symbol == TTT_X || player_symbol == TTT_O;
}


void display_player(Player &player){
    cout << convert_number_toWords(player._position) << " player has the following [ " << " Player Name : " << player._name << ", Player Symbol : " << char(player._players_symbol) << " ]" << endl;
}

void print_board(char board[][COLUMN_LENGTH]){
    cout << "-------------------------------------------------\n";
    for(int row = 0; row < ROW_LENGTH; row++){
        cout << "|";
        
        for(int column = 0; column < COLUMN_LENGTH; column++){
            if(column > 0) {cout << "|";}
            cout << "\t" <<  board[row][column] << "\t" ;
        }
        
        cout << "| \n";
        cout << "-------------------------------------------------\n";
    }
}

int generate_random_num_within_range(int high, int low){
    return rand() % (high - low) +  low;
}

int* generate_random_row_column(char board[][COLUMN_LENGTH]){
    int row = generate_random_num_within_range(4,1) ;
    int column = generate_random_num_within_range(4,1);
    
    //check for computers stupidity let it act smart.
    while(!valid(row, column) || !is_position_empty(row, column, board)){
        row = generate_random_num_within_range(4,1) ;
        column = generate_random_num_within_range(4,1);
    }
    
    int *rol_and_col = new int[2];
    
    rol_and_col[0] = row ;
    rol_and_col[1] = column;
    
    return rol_and_col;
}


bool is_position_empty(int row , int column, char board[][COLUMN_LENGTH]){
    
    if(board[row - 1 ][column - 1] == TTT_O){
        return false;
    }
    
    if(board[row - 1][column - 1] == TTT_X){
        return false;
    }
    
    return true;
}


void play(int row , int column, TicTacToeOption players_symbol,char board[][COLUMN_LENGTH]){
    
    board[row - 1][column - 1] = players_symbol;
    
}

bool no_winner(Player& player, char board[][COLUMN_LENGTH]){
    for(int row = 0 ; row < ROW_LENGTH ; row++ ){
        if(board[row][0] == board[row][1] && board[row][0] == board[row][2] && board[row][2] == board[row][1]){
            if(board[row][0] != TTT_EMPTY){
                
                if (player._players_symbol == board[row][0]) {
                    player._winner = true;
                }
                return false;
            }
        }
        
    }
    
    for(int column = 0; column < COLUMN_LENGTH; column++){
        if(board[0][column] == board[1][column] && board[0][column] == board[2][column] && board[2][column] == board[1][column]){
            if(board[0][column] != TTT_EMPTY){
                if (player._players_symbol == board[0][column]) {
                    player._winner = true;
                    return false;
                }
    
            }
        }
    }
    
    if(board[0][0] == board[1][1] && board[0][0] == board[2][2] && board[2][2] == board[1][1]){
        if(board[0][0] != TTT_EMPTY){
            if (player._players_symbol == board[0][0]) {
                player._winner = true;
                return false;
            }
        }
    }
    
    if(board[0][2] == board[1][1] && board[0][2] == board[2][0] && board[2][0] == board[1][1]){
        if(board[0][0] != TTT_EMPTY){
            if (player._players_symbol == board[0][2]) {
                player._winner = true;
                return false;
            }
            
        }
    }
    
    
    return true;
}