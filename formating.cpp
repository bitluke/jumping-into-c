#include <iostream>
#include <iomanip>

using namespace std;

int main(){
cout  << setw(7) << "five" << endl;
	cout << setw(10) << "ten" << "eight" << "twenty-four" << endl;
	cout << setw(9) << "nine" << endl;
	cout << setfill('-')  << setw(8) << "eight" << endl;
	
	cout << setw(7) << "seven" << endl;
	cout << setw(6) << left  << "six" << endl;
	cout << setw(5) << "five" << endl;
	cout << setw(4) << "four" << endl;
	cout << setw(3) << "three" << endl;
	cout << setw(2) << "two" << endl;
	cout << setw(1) << "one" << endl;
	cout << setw(0) << "zero" << endl;
}
