#include <iostream>

using namespace std;

int main(){
 
        int a;
        int b;
        int c;
        int test_array[2];
	int d;	

        cout << "&a: " << &a << endl;
        cout << "&b: " << &b << endl;
        cout << "&c: " <<  &c << endl;
        cout << "&test_array: " << &test_array << endl;
	cout << "&d: " <<  &d << endl;

	if(&a < &b){
		cout << "&a < &b" << endl;
		cout << &a - &b << endl;
		cout << "&a: " << &a << endl;
        	cout << "&b: " << &b << endl;

	}else{
		cout << "&a > &b" << endl;
		cout << "diff : "  << &a - &b << endl; 
		cout << "&a: " << &a << endl;
        	cout << "&b: " << &b << endl;	
	
	 }

}
