#include <iostream>

using namespace std;

void free_memory(int &pint);


int main(){
	int* p_int = new int;
	int p_array[4] = {1,2,2,3};
	*p_int = 'a';
	cout << "*p_int : " << *p_int << " p_int: " << p_int << endl; 
	cout << "*p_array : " << p_array << " p_array: " << p_array << endl;
	free_memory(p_int);
	cout << "After deleting *p_int : " << *p_int << " p_int: " << p_int << endl;
}

void free_memory(int &pint){
	delete &pint;
	pint = NULL;
}
