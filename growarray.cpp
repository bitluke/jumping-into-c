#include <iostream>

using namespace std;

int *grow_array(int *p_array, int current_size);

int main(){
	int next_element = 0;
	int size = 10;

	int *p_values = new int[size];
	int value = 0;

	cout << "enter a number";
	cin >> value;

	while(value > 0){

		if(size == next_element + 1){
			p_values = grow_array(p_values, size);
		}

		p_values[next_element] = value;
		cout << "enter a number or 0 to exit" << endl;
		cin >> value; 
	}
	

}

int *grow_array(int *p_array, int current_size){
	int *p_new_array = new int [current_size * 2];
	for(int index = 0 ; index < current_size ; index++){

		p_new_array[index] = p_array[index];

	}
	delete p_array;
	return p_new_array;
}
