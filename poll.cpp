#include <iostream>

using namespace std;

void create_question_answers(string question, string option1, string option2, string option3){
	cout << question << endl ;
	cout << "1 : " << option1 << endl;
	cout << "2 : " << option2 << endl;
	cout << "3 : " << option3 << endl;
}


int main(){
	//ask question 
	
	string question = "";
	cout << "Please enter your question \n";
	getline(cin, question, '\n');

	//provide answers in form of menu
	cout << "Enter possible answers \n";
	string option_one = "";
	string option_two = "";
	string option_three = "";
	cout << "Enter option 1 \n";
	getline(cin, option_one, '\n');
	cout << "Enter option 2 \n";
	getline(cin, option_two, '\n');
	cout << "Enter option 3 \n";	
	getline(cin, option_three, '\n');
	
	int answer = -1;
	int total_of_option1 = 0;
	int total_of_option2 = 0;
	int total_of_option3 = 0;
	
	create_question_answers(question,option_one, option_two, option_three);
	cout << "Your answer : \n";
	cin >> answer;

	//get answers
	while(answer != 0){
		
		if(answer == 1){
	
			++total_of_option1;
	
		}else if(answer == 2){
	
			++total_of_option2;
	
		}else if(answer == 3){
	
			++total_of_option3;
	
		}	


		create_question_answers(question, option_one, option_two, option_three);
        	cout << "Your answer : \n";
		cin >> answer;	
	}

	cout << " Total : " << " total 1 " << total_of_option1 << " total 2 " << total_of_option2 <<  " total 3 " << total_of_option3; 
} 
