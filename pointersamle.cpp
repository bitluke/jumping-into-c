#include <iostream>

using namespace std;

int main(){

	int x = 5232;

	int *p_x = &x;

	cout << "p_x is : " << p_x << endl;

	cout << "*p_x is : " << *p_x << endl;

	cout << "&x is : " << &x << endl;
	
	*p_x = 4;

	cout << "*p_x new value " << *p_x << endl;

	cout << "p_x new value : " << p_x << endl; 

	cout << "&p_x is : " << &p_x << endl;

	int **p_p_x = &p_x;
	
	cout << "**p_p_x is : " << **p_p_x << endl; 

	cout << "*p_p_x is : " << *p_p_x << endl;

	int *p_b = 4;

	cout << "type of " << typeof(p_x) << endl;
}
