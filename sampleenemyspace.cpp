#include <iostream>



using namespace std;

struct EnemySpaceShip{
	int y_coordinate;
	int x_coordinate;
	int weapon_power;
};


EnemySpaceShip getNewEnemy();
EnemySpaceShip upgrade_weapons(EnemySpaceShip enemySpaceShip);

int main(){
	EnemySpaceShip enemy = getNewEnemy();
	enemy = upgrade_weapons(enemy);
	cout << "Enemy weapon upgraded to " << enemy.weapon_power ;	
}

EnemySpaceShip upgrade_weapons(EnemySpaceShip enemySpaceShip){
	enemySpaceShip.weapon_power++;
	return enemySpaceShip;
}

EnemySpaceShip getNewEnemy(){
	EnemySpaceShip ship;
	ship.x_coordinate = 0;
	ship.y_coordinate = 0;
	ship.weapon_power = 20;
	return ship;
}
