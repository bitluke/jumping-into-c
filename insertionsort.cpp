#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

const int NUMBER_SIZE = 100;

void display(int array_of_numbers[], int size_of_array);
void sort(int numbers[],int size_of_numers);
int find_index_of_smallest(int numbers[], int size_numbers, int starting_index);
void rearrange_smallest_number(int numbers[], int  index, int index_of_smallest_number);

int main(){
	int numbers[10];
	srand(time(NULL));

	for(int index = 0; index < NUMBER_SIZE; index++){
		numbers[index] = rand() % 100;
	}

	cout << "original array : ";
	display(numbers, NUMBER_SIZE);

	sort(numbers, NUMBER_SIZE);

	cout << "sorted array   : ";
	display(numbers, NUMBER_SIZE);
}

void display(int numbers[], int size_of_numbers){
	cout << "{";

	for(int index = 0; index < size_of_numbers; index++){
		
		if(index > 0 ){
			cout << "," ;
		}
		cout << numbers[index];
	}

	cout << "}" << endl ;
}

void sort(int numbers[], int size_of_numbers){
	for(int index = 0; index < size_of_numbers; ++index){
		int index_of_smallest_number = find_index_of_smallest(numbers, size_of_numbers, index);
		rearrange_smallest_number(numbers, index, index_of_smallest_number);
	}
}


int find_index_of_smallest(int numbers[], int size_numbers, int starting_index){
	int index_of_smallest_number = starting_index;
	
	for(int index = starting_index + 1 ; index < size_numbers; index++){
		
		if(numbers[index] < numbers[index_of_smallest_number]){
			index_of_smallest_number = index;
		}		

	}

	return index_of_smallest_number;
}

void rearrange_smallest_number(int numbers[], int  index, int index_of_smallest_number){
	int temp = numbers[index];
	numbers[index] = numbers[index_of_smallest_number];
	numbers[index_of_smallest_number] = temp;
}
