#include<iostream>

using namespace std;


struct Person{
	string name;
	string lastname;
};

void retur_names(string &name, string &lastname);
void retur_names_pointer(string *name, string *lastname);

int main(){
	cout << "using Reference Parameter \n";
	string name , lastname;

	cout << "Before address function: &name: "<< &name << " &lastname: " << &lastname << endl ;
	retur_names(name, lastname);
	cout << "Entered parameters in function displayed in main " << endl;
        cout << " name: "<< name << " last name " << lastname << endl;
	cout << "After address function : &name: "<< &name << " &lastname: " << &lastname << endl ;

	cout << "***************************************************************************** \n";	

	cout << "Before pointer function : &name: "<< &name << " &lastname: " << &lastname << endl ;
        retur_names_pointer(&name, &lastname);
        cout << "Entered parameters in function displayed in main " << endl;
        cout << " name: "<< name << " lastname: " << lastname << endl;
        cout << "After pointer function : &name: "<< &name << " &lastname " << &lastname << endl ;



}

void retur_names(string &name, string &lastname){
	cout << "\n retur_names_pointer  \n";
	cout << "Function ref call before change : name: "<< name << "lastname: " << lastname << endl ;
        cout << "Function ref call before change : &name: "<< &name << " &lastname: " << &lastname << endl ;
	//cout << "Function ref call before change : name pointer "<< *name << " last name address " << *lastname << endl ;	

	cout << " enter first name ";
        cin >> name;
        cout << " enter lastname name ";
        cin >> lastname;

	cout << "Function ref call : name: "<< name << "lastname: " << lastname << endl ;
	//cout << "Function ref call : *name: "<< *name << " *lastname: " << *lastname << endl ;
	cout << "Function ref call : &name: "<< &name << " &lastname: " << &lastname << endl ;
}


void retur_names_pointer(string *name, string *lastname){
	cout << "\n retur_names_pointer \n";
	cout << "In Function pointer call before change : name: "<< name << " lastname: " << lastname << endl ;
        cout << "In Function pointer call before change : *name: "<< *name << " *lastname: " << *lastname << endl ;
        cout << "In Function pointer call before change : &name: "<< &name << " &lastname: " << &lastname << endl ;        

	cout << " enter first name ";
        cin >> *name;
        cout << " enter lastname name ";
        cin >> *lastname;

        cout << "In Function pointer call : name: "<< name << " lastname: " << lastname << endl ;
        cout << "In Function pointer call : *name: "<< *name << " *lastname: " << *lastname << endl ;
	cout << "In Function pointer call : &name: "<< &name << " &lastname: " << &lastname << endl ;
}

