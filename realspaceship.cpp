#include <iostream>



using namespace std;

struct EnemySpaceShip{
	int y_coordinate;
	int x_coordinate;
	int weapon_power;
};

const int WIDTH_OF_SCREEN = 768;
const int HEIGHT_OF_SCREEN = 1024;

EnemySpaceShip getNewEnemy();
EnemySpaceShip upgrade_weapons(EnemySpaceShip enemySpaceShip);

int main(){
	EnemySpaceShip enemy = getNewEnemy();
	enemy = upgrade_weapons(enemy);
	cout << "Enemy weapon upgraded to " << enemy.weapon_power ;	
}

EnemySpaceShip upgrade_weapons(EnemySpaceShip enemySpaceShip){
	enemySpaceShip.weapon_power++;
	return enemySpaceShip;
}

EnemySpaceShip update_position(EnemySpaceShip enemySpaceShip, int x, int y){
	
	enemySpaceShip.x_coordinate += x;

	if(enemySpaceShip.x_coordinate > WIDTH_OF_SCREEN){
		enemySpaceShip.x_coordinate = WIDTH_OF_SCREEN;
	}
	
	if(enemySpaceShip.x_coordinate < 0){ 
                enemySpaceShip.x_coordinate = 0;
        }

	enemySpaceShip.y_coordinate += y;

	if(enemySpaceShip.y_coordinate > HEIGHT_OF_SCREEN){
		enemySpaceShip.y_coordinate = HEIGHT_OF_SCREEN;
	}
	
	if(enemySpaceShip.y_coordinate < 0){
                enemySpaceShip.y_coordinate = 0;
        }

	return enemySpaceShip;

}

EnemySpaceShip getNewEnemy(){
	EnemySpaceShip ship;
	ship.x_coordinate = 0;
	ship.y_coordinate = 0;
	ship.weapon_power = 20;
	return ship;
}
