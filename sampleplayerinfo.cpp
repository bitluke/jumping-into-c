#include <iostream>



using namespace std;

struct PlayerInfo{
	int skill_level;
	string name;
};


int main(){
	PlayerInfo player_infos[5];

	for(int count = 0; count < 5; count++){
		cout << "Please enter the name of player " << count << endl;
		cin >> player_infos[count].name ;
		cout << "Please enter the skill level for" <<  player_infos[count].name << endl; 
		cin >> player_infos[count].skill_level;
	}

	for(int count = 0; count < 5; count ++){
		cout << player_infos[count].name << " is at skill level " << player_infos[count].skill_level << endl;
	}
}
