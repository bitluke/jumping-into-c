#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

enum TicTacToeOption { TTT_EMPTY = '-', TTT_O = 'O', TTT_X = 'X'};

const int ROW_LENGTH = 3;
const int COLUMN_LENGTH = 3;

void init_board();
int random_within_range(int high, int low);
//void generate_random_row_column();
int *generate_random_row_column();
void play(int row , int column, char players_symbol );
void play(int row , int column, TicTacToeOption players_option );
bool is_position_empty(int row , int column);
void display_winner();
bool no_winner();
string  convert_to_winner(int number);
void print_board();
bool valid(int row, int column);


char board[ROW_LENGTH][COLUMN_LENGTH];

int winner = -2;
char player_a_name = ' ';
char player_b_name = ' ';

int main(){
	srand(time(NULL));	
	
	//Set up board.
	init_board();

	//player a plays
	do{

		cout << "Player A choose \"X\" or \"x\" or \"O\" or \"o\" : ";
		cin >> player_a_name ;

	}while(player_a_name != 'X' & player_a_name != 'x'  &  player_a_name != 'O' & player_a_name != 'o');

	cout << "Want to play against the computer ? Y/n (Y is the default) ";
	char will_play_computer = 'Y';
	cin >> will_play_computer;

	//player a plays
	if(player_a_name == 'X' | player_a_name == 'x'){

		player_b_name = 'O';
	
		if(will_play_computer){
			cout << "Computer chose  \'O\' ";
		}else {
			cout << "Player B \'O\' has been choosen for you ";
		}
	}else {

		player_b_name = 'X';
		if(will_play_computer){
                        cout << "Computer chose  \'X\' ";
                }else {
                        cout << "Player B \'X\' has been choosen for you ";
                }

	}
	
	string player_name = "A";
	
	cout << endl <<  "Time to play" << endl;
	cout << "player  " << player_name  << " your turn" << endl;

	cout << " specify position in terms of row and column \n ";
	cout << " starting from 1 and the last possible number is  3 \n ";
	cout << " row = 1 , row =  3 , column = 1 , column = 3 \n ";

	int row = 0; int column = 0;

	do{
                cout << "Enter row : " ;
                cin >> row;
                cout << "Enter column : " ;
        	cin >> column;

        }while(!valid(row, column));


	char next_player = player_a_name;

	play(row, column, next_player);
	print_board();
	
	if(next_player == player_a_name){
	
		if(will_play_computer){
			player_name = "Computer";
		}else{	
			player_name = "B";
		}

       		 next_player = player_b_name;

        }else {
		player_name = "A"; //player is always human.
                next_player = player_a_name;
        }

	int number_of_plays = 1;

	while(no_winner() && number_of_plays < 9){

		
		do{
			if(player_name == "A" ){
				cout << "player " << player_name << "  your turn " << endl;
				cout << "Enter row : " ;
                		cin >> row;
                		cout << "Enter column : " ;
                		cin >> column;
			} else {
				if(will_play_computer){
					int* comp_row_col =  generate_random_row_column(); 
					row = comp_row_col[0] ;
					column = comp_row_col[1];
					cout << "Computer played the following : row " << comp_row_col[0] << " colum : " << comp_row_col[1] << endl;  
				}else {
					cout << "player " << player_name << "  your turn " << endl;
                        		cout << "Enter row : " ;
                        		cin >> row;
                        		cout << "Enter column : " ;
                        		cin >> column;
				}			
			}

		}while(!valid(row, column) || !is_position_empty(row, column));

		play(row, column, next_player);
		
		print_board();

		if(next_player == player_a_name){
			player_name = "B";
                        next_player = player_b_name;

                }else {
			player_name = "A";
                        next_player = player_a_name;
                }
	

		number_of_plays++;
	}	

	display_winner();
}


void init_board(){

	for(int row = 0; row < ROW_LENGTH; row++){
		for(int column = 0; column < COLUMN_LENGTH; column++){
			board[row][column] = TTT_EMPTY;
		}
	} 
}

void play(int row , int column, char players_symbol ){
	if(players_symbol == 'X'){
		 play(row, column, TTT_X);
	}else {
		play(row, column, TTT_O);
	}
}

void play(int row , int column, TicTacToeOption players_option ){

	 board[row - 1][column - 1] = players_option;

}

bool no_winner(){
	for(int row = 0 ; row < ROW_LENGTH ; row++ ){
		if(board[row][0] == board[row][1] && board[row][0] == board[row][2] && board[row][2] == board[row][1]){
			if(board[row][0] != TTT_EMPTY){
				winner = board[row][0];
				return false;
			}
		}

	}

	 for(int column = 0; column < COLUMN_LENGTH; column++){
                         if(board[0][column] == board[1][column] && board[0][column] == board[2][column] && board[2][column] == board[1][column]){
                                 if(board[0][column] != TTT_EMPTY){
                                        winner = board[0][column];
                                        return false;
                                }
                     }
          } 

	if(board[0][0] == board[1][1] && board[0][0] == board[2][2] && board[2][2] == board[1][1]){
        	if(board[0][0] != TTT_EMPTY){
                	 winner = board[0][0];                                        
                         return false;
               	 }
        }

	if(board[0][1] == board[1][1] && board[0][1] == board[0][2] && board[0][2] == board[1][1]){
                if(board[0][1] != TTT_EMPTY){
                         winner = board[0][1];
                         return false;
                 }
        }

	return true;
}

bool is_position_empty(int row , int column){

	if(board[row - 1 ][column - 1] == TTT_O){
		return false;
	}

	if(board[row - 1][column - 1] == TTT_X){ 
                return false;
        }
         
       return true;
}

void display_winner(){

	cout << convert_to_winner(winner);

}

string  convert_to_winner(int number){

	if(winner == TTT_X){

		if(player_a_name == 'X'){
			return "the winner is Player A \n";
		}else{
			return "the winner is Player B \n";	
		}

	}else if(winner == TTT_O){

		if(player_a_name == 'O'){ 
                        return "the winner is Player A \n";
                }else{ 
                        return "the winner is Player B \n";
                }

	}else {
		return  "It is a tie : You losers :D !!! ";
	}

}

bool valid(int row, int column){
        if(row >= 1 && row <= 3 ){
		if(column >= 1 && column <= 3){
			return true;
        	}
        }
	return false;
}


void print_board(){
	cout << "-------------------------------------------------\n";
	for(int row = 0; row < ROW_LENGTH; row++){
		cout << "|";
 
                for(int column = 0; column < COLUMN_LENGTH; column++){
			if(column > 0) {cout << "|";}
                       	cout << "\t" <<  board[row][column] << "\t" ;
                }

		cout << "| \n";
		cout << "-------------------------------------------------\n";
        }
}

int *generate_random_row_column(){
	int row = random_within_range(3,1) ;
	int column = random_within_range(3,1);

	//check the for computers stupidity let it act smart.
	while(!valid(row, column) || !is_position_empty(row, column)){
		 row = random_within_range(3,1) ;
        	 column = random_within_range(3,1);
	}
	
	int *rol_and_col = NULL;

	rol_and_col[0] = row ;
	rol_and_col[1] = column;
	
	return rol_and_col;
//	return ;
}	

int random_within_range(int high, int low){
	return rand() % (high - low) +  low;
}
