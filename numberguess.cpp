#include <cstdlib>
#include <iostream>
#include <ctime>

using namespace std;
int rand_range(int high , int low);
int create_guess();

int main(){

	srand(time(NULL));
	long computer_guess = create_guess();
 
	long input_guess;
	cout << "enter your guess";
	cin >> input_guess;
	int count = 0;

	while(input_guess != computer_guess){
		
		if(input_guess > computer_guess){
			cout << "Your guess is higher :";
			cout << "enter another guess";
			cin >> input_guess;
		}else if (input_guess < computer_guess){
			cout << "Your guess is lower";
			cout << "enter another guess : ";
                        cin >> input_guess;
		}
		
		//count
		count++;
	}

	//ouput the numbers.
	cout  << "Number of guesses  : " << count << endl;

}

int create_guess(){
	return rand_range(100, 1);
}

int rand_range(int high , int low){
	return rand() % (high - low) + low;
}
