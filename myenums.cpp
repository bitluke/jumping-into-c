#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;


enum RainbowColor{
	RC_RED, RC_ORANGE, RC_YELLOW, RC_GREEN, RC_BLUE, RC_INDIGO, RC_VIOLET
};

enum RainbowColor_WithNumbers{ 
        RED = 1, ORANGE = 4, YELLOW = 5, GREEN = 6, BLUE = 7 , INDIGO = 8, VIOLET =89 
};

int rand_range(int low,int high){
	return rand() % (high - low) +  low;
}


int main(){

	RainbowColor favourite = RC_ORANGE;

	//generate a number

	srand(time(NULL));

	cout << "rand number: " << rand() << endl;
	
	for(int i = 0 ; i < 1000; i++){
		cout << rand_range(4, 10) << endl;
	}
}
